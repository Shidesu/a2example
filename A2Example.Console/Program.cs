﻿// See https://aka.ms/new-console-template for more information

using A2Example.Core.Models;
using A2Example.Core.Providers;
using A2Example.Core.Repositories;

Console.WriteLine("Json Provider");
var studentRepository = new StudentRepository(new JsonDataProvider<Student>());

studentRepository.Add(new Student
{
    FirstName = "Kao",
    LastName = "Otoko",
    CesiYear = CesiYear.Alumni,
});

Console.WriteLine(string.Join('\n', studentRepository.GetAll()));

studentRepository.Save();


Console.WriteLine("------------------------");
Console.WriteLine("InMemory Provider");

studentRepository = new StudentRepository(new MemoryDataProvider<Student>());

studentRepository.Add(new Student
{
    FirstName = "Kao",
    LastName = "Otoko",
    CesiYear = CesiYear.Alumni,
});

Console.WriteLine(string.Join('\n', studentRepository.GetAll()));

studentRepository.Save();

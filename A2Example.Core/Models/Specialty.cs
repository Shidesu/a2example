﻿namespace A2Example.Core.Models;

public enum Specialty
{
    IT,
    Generalist,
}
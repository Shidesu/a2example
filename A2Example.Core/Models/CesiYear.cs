﻿namespace A2Example.Core.Models;

public enum CesiYear
{
    A1,
    A2,
    A3,
    A4,
    A5,
    Alumni,
}
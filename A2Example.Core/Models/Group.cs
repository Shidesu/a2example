﻿namespace A2Example.Core.Models;

public class Group
{
    public Guid Id { get; set; } = Guid.NewGuid();
    public Specialty Specialty { get; set; }
}
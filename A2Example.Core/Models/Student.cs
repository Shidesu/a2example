﻿namespace A2Example.Core.Models;

public class Student
{
    public Guid Id { get; set; } = Guid.NewGuid();
    public required string FirstName { get; set; }
    public required string LastName { get; set; }
    public required CesiYear CesiYear { get; set; }
    public string FullName => $"{FirstName} {LastName}";
    public Guid GroupId { get; set; }

    public override string ToString()
    {
        return $"{Id} - {FullName} - {CesiYear}";
    }
}
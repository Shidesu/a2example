﻿namespace A2Example.Core.Providers;

public interface IDataProvider<T>
{
    public void Load();
    public void Save();
    public ICollection<T> Data { get; }
}
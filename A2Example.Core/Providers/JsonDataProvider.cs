﻿using System.Diagnostics.CodeAnalysis;
using System.Text.Json;

namespace A2Example.Core.Providers;

public class JsonDataProvider<T> : IDataProvider<T>
{
    private readonly string _filePath = typeof(T).Name;
    public ICollection<T> Data { get; private set; } = Array.Empty<T>();
    
    public void Load()
    {
        using var file = File.Open(_filePath, FileMode.OpenOrCreate, FileAccess.Read);
        try
        {
            Data = JsonSerializer.Deserialize<ICollection<T>>(file) ?? [];
        }
        catch
        {
            Data = [];
        }
    }

    public void Save()
    {
        var json = JsonSerializer.Serialize(Data);
        File.WriteAllText(_filePath, json);
    }
}
﻿namespace A2Example.Core.Providers;

public class MemoryDataProvider<T> : IDataProvider<T>
{
    public void Load()
    {
    }

    public void Save()
    {
    }

    public ICollection<T> Data { get; } = new List<T>();
}
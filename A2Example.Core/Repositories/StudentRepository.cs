﻿using A2Example.Core.Models;
using A2Example.Core.Providers;
using A2Example.Core.Services;

namespace A2Example.Core.Repositories;

public class StudentRepository : IRepository<Student>
{
    private readonly IDataProvider<Student> _studentProvider;

    public StudentRepository(IDataProvider<Student> studentProvider)
    {
        _studentProvider = studentProvider;
        _studentProvider.Load();
    }
    
    public IEnumerable<Student> GetAll()
    {
        return _studentProvider.Data;
    }

    public Student? GetByIdOrDefault(Guid id)
    {
        return _studentProvider.Data.FirstOrDefault(s => s.Id == id);
    }

    public void Add(Student student)
    {
        _studentProvider.Data.Add(student);
    }

    public void Save() => _studentProvider.Save();
}
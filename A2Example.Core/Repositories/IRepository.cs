﻿namespace A2Example.Core.Services;

public interface IRepository<T>
{
    public IEnumerable<T> GetAll();
    public T? GetByIdOrDefault(Guid id);
    public void Add(T value);
}